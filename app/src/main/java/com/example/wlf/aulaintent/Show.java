
package com.example.wlf.aulaintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class Show extends AppCompatActivity {

    private TextView exibeID;
    private TextView exbieNOME;
    private TextView exibeIDADE;
    private TextView exibeGENERO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        exibeID = (TextView) findViewById(R.id.exibe_ID);
        exbieNOME = (TextView) findViewById(R.id.exibe_Nome);
        exibeIDADE = (TextView) findViewById(R.id.exibe_Idade);
        exibeGENERO = (TextView) findViewById(R.id.exibe_Genero);

        //Getting the object from MainActivity

        Intent intent = getIntent();

        Customer exibe = (Customer) intent.getSerializableExtra("customer");

        //Showing the fields

        exibeID.setText(String.valueOf(exibe.getId()));
        exbieNOME.setText(String.valueOf(exibe.getNome()));
        exibeIDADE.setText(String.valueOf(exibe.getIdade()));
        exibeGENERO.setText(String.valueOf(exibe.getGenero()));
    }

}
