package com.example.wlf.aulaintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Serializable {

    private EditText id;
    private EditText nome;
    private EditText idade;
    private EditText genero;
    private Button salvar;

    private Customer customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        id      = findViewById(R.id.edit_ID);
        nome    = findViewById(R.id.edit_NOME);
        idade   = findViewById(R.id.edit_IDADE);
        genero  = findViewById(R.id.edit_GENERO);
        salvar  = findViewById(R.id.btn_SAVE);

        salvar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int a = view.getId();
        if (a == R.id.btn_SAVE) {

            //logic button save

            customer = new Customer(); //new object

            id.getText();
            nome.getText();
            idade.getText();
            genero.getText();
            salvar.getText();

            //get the parameters

            customer.setId(Integer.parseInt(id.getText().toString()));
            customer.setNome(nome.getText().toString());
            customer.setIdade(Integer.parseInt(idade.getText().toString()));
            customer.setGenero(genero.getText().toString());

            //Sending the object for Show.clas

            Intent intent = new Intent(this, Show.class);

            intent.putExtra("customer", customer);

            startActivity(intent);
        }
    }

}


